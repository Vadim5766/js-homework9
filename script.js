function massivElement(massiv, parrentElement = document.body) {
  let list = document.createElement("ul");
  let itemList;
 let newMassiv = massiv.map((item) => {
  if ( Array.isArray(item)) {
    massivElement(item, itemList)
  }else{
      itemList = document.createElement("li");
      itemList.textContent = `${item}`;
      itemList.style.cssText = "list-style-type:none";
  }
  return itemList;
  })
  list.append(...newMassiv);
  parrentElement.append(list);
}
massivElement([
  "hello",
  8,
  "Kiev",
  ["hi","JS"],
  "Kharkiv",
  "Odessa",
  "Lviv",
]);

// setTimeout(clear,3000);
function clear() {
  document.body.innerHTML = "";
}